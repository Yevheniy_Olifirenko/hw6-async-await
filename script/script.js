let button = document.querySelector(".divBtn");
let userInfo = document.querySelector(".info");

async function findByIP() {
    try {
        const userIP = await fetch("https://api.ipify.org/?format=json");
        const infoIP = await userIP.json();

        const userApiIP = await fetch(`http://ip-api.com/json/${infoIP.ip}?fields=continent,country,regionName,city,district`);
        const infoApiIP = await userApiIP.json();

        const resultInfo = userInfo;
        resultInfo.innerHTML = `
            <p><strong>continent: </strong> ${infoApiIP.continent}</p>
            <p><strong>country: </strong> ${infoApiIP.country}</p>
            <p><strong>regionName: </strong> ${infoApiIP.regionName}</p>
            <p><strong>city: </strong> ${infoApiIP.city}</p>
            <p><strong>district: </strong> ${infoApiIP.district}</p>
        `;
    } catch (error) {
        console.error("Error getting IP address", error);
    }
}

button.addEventListener('click', () => {
    findByIP();
});